# -*- coding: utf-8 -*-

# {{{ Codebase
py = {}

import os

blogdir = "/home/catbat/ada/"
# }}}

# {{{ General
py["base_url"] = ""

py["blog_title"] = "exercise in futility"

py["blog_description"] = "words"

py["blog_author"] = "end"

py["blog_email"] = "none@none.none"

py["blog_rights"] = '&copy; 2019</a>'

py["blog_language"] = "en"
py["blog_encoding"] = "utf-8"

py["datadir"] = os.path.join(blogdir, "entries")

py["flavourdir"] = os.path.join(blogdir, "flavours")
py["default_flavour"] = "html"

py["ignore_directories"] = []
py["depth"] = 0

py["num_entries"] = 21
# }}}

# {{{ Plugins
py["plugin_dirs"] = [os.path.join(blogdir, "plugins")]

py["load_plugins"] = ["Pyblosxom.plugins.pyfilenamemtime", "Pyblosxom.plugins.markdown_parser", "Pyblosxom.plugins.pycategories"]

# Markdown parser plugin
py['parser'] = 'markdown'

# PyCategories plugin
py["category_start"] = "<ul>"
py["category_begin"] = "<li>cats</li>"
py["category_item"] = '<li><a href="%(base_url)s/%(category_urlencoded)s">%(category)s ( %(count)s )</a></li>'
py["category_end"] = ""
py["category_finish"] = "</ul>"


# }}}


# {{{ Static rendering
py["static_dir"] = "/var/www/html"

py["static_flavours"] = ["html"]

py["static_index_flavours"] = ["html"]

py["static_monthnames"] = 1

py["static_monthnumbers"] = 0
# }}}
# }}}
