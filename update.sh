#!/bin/bash

blog_dir="${HOME}/ada"
www_dir="/var/www/html"
branch="master"

cd "${blog_dir}"

git pull origin "${branch}" | grep Updating

if [ $? -eq 0 ]; then
  source bin/activate
  pyblosxom-cmd  staticrender
  cp -ru pages/* "${www_dir}/"
fi
